Gem::Specification.new do |s|
  s.name        = 'oneprovider-api'
  s.version     = '0.0.1'
  s.date        = '2018-03-29'
  s.summary     = "OneProvider API"
  s.description = "A simple ruby helper for interacting with OneProvider's API"
  s.authors     = ["Rúben Enes"]
  s.email       = 'ruben@enes.io'
  s.files       = `git ls-files`.split($/)
  s.require_paths = ["lib"]
  s.homepage    =
    'https://bitbucket.org/rdpse/oneprovider-api'
  s.license       = 'MIT'
end
