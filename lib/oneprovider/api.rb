require 'digest/sha1'
require 'net/http'
require 'net/https'
require 'json'
require 'uri'

module OneProvider

  class APIError < StandardError; end

  class API
    DEFAULT_API_URL = "https://api.oneprovider.com"

    attr_accessor :api_url

    class << self

      def build_http_object(host, port)
        if ENV['https_proxy']
          proxy = URI.parse(ENV['https_proxy'])
          Net::HTTP::Proxy(proxy.host, proxy.port).new(host, port)
        else
          Net::HTTP.new(host, port)
        end
      end
    end

    def initialize(api_key, client_key, api_url = nil)
      @api_url = api_url || DEFAULT_API_URL
      @api_key, @client_key = api_key, client_key
    end

    [:get, :post, :put, :delete].each do |method|
      define_method method do |endpoint, payload = nil|
        raise APIError, "Invalid endpoint #{endpoint}, should match '/server|vm/.*'" unless %r{^/(server|vm)/.*$}.match(endpoint)

        url = @api_url + endpoint
        uri = URI.parse(url)

        # encode payload 
        if payload
          payload = URI.encode_www_form(payload)
          method == :get ? uri.query = payload : data = payload
        end

        # create oneProvider authentication headers
        headers = {
          "API_KEY" => @api_key,
          "CLIENT_KEY" => @client_key,
          "X-Pretty-JSON" => "1"
        }

        # instanciate Net::HTTP::Get, Post, Put or Delete class
        request_uri = uri.path
        request_uri += '?' + uri.query if uri.query
        request = Net::HTTP.const_get(method.capitalize).new(request_uri, initheader = headers)
        request.body = data

        http = API.build_http_object(uri.host, uri.port)
        http.use_ssl = true
        response = http.request(request)
        result = response.body == "null" ? nil : JSON.parse(response.body)

        unless response.is_a?(Net::HTTPSuccess)
          raise APIError, "Error querying #{endpoint}: #{result["message"]}"
        end

        result
      end
    end

  end
end
